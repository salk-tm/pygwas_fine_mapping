#!/usr/bin/env python3
#
#   usage:
#       add_intersect_feat_to_bed.py -bed <filepath> -feat <filepath>
#
#   description:
#       Uses bedtools intersect to find features from -feat that intersect
#       features in -bed and adds their names (from column 4) as an attribute
#       in column 9 under key otherFeature.

import sys
import os
from subprocess import run

args = sys.argv

bedpath = args[args.index("-bed") + 1]
featpath = args[args.index("-feat") + 1]

bedbase = os.path.basename(bedpath).rstrip('.bed')

if not os.path.exists("snps_intersect_feats"):
    os.mkdir("snps_intersect_feats")
tempbed = f"snps_intersect_feats/{bedbase}.intersect_feat.bed"

with open(tempbed, "w") as outf:
    run(["bedtools", "intersect", "-wa", "-wb", "-a", bedpath, "-b", featpath], stdout = outf)

# Sort intersect file
run(["sort", "-o", tempbed, "-k1,1", "-k2,2n", tempbed])

newBedLines = dict()
with open(tempbed) as inf:
    lastsnp = None
    lastbedline = None
    feats = list()
    for line in inf:
        # Get fields values
        linesplit = line.strip().split("\t")
        intersectedfeat = linesplit[12]
        snp = linesplit[3]
        # There can be multiple lines per snp, one for each feature it intersects
        # The first line in the file:
        if lastsnp == None:
            # Get intersected feature name
            lastsnp = snp
            # Save snp bed line
            lastbedline = linesplit[:9]
            # Add feat to list for this snp
            feats.append(intersectedfeat)
        # This snp is the same as the last line
        elif lastsnp == snp:
            # Add intersected feature to list of feats for this snp
            feats.append(intersectedfeat)
        # This snp is different from the last
        else:
            # Add new key=value for features to the end of the existing attributes field
            newattr = f"otherFeature={','.join(feats)}"
            lastbedline[8] = f"{lastbedline[8].rstrip(';')};{newattr}"
            newline = "\t".join(lastbedline)
            # Save new line in dict
            newBedLines[lastsnp] = newline
            # Re-initialize feats list and lastsnp
            feats = [intersectedfeat]
            lastsnp = snp
            lastbedline = linesplit[:9]
    # For last snp:
    # Add new key=value for features to the end of the existing attributes field
    newattr = f"otherFeature={','.join(feats)}"
    lastbedline[8] = f"{lastbedline[8].rstrip(';')};{newattr}"
    newline = "\t".join(lastbedline)
    # Save new line in dict
    newBedLines[lastsnp] = newline
        
# Print all lines from input bed (some may not have been in the intersect file)
with open(bedpath) as inf:
    for line in inf:
        linesplit = line.strip().split("\t")
        if linesplit[3] in newBedLines:
            print(newBedLines[linesplit[3]])
        # If no features add otherFeature=none to attr field
        else:
            linesplit[8] = f"{linesplit[8].rstrip(';')};otherFeature=none"
            print("\t".join(linesplit))

## Example -bed
#1	7166125	7166126	1_7166126	0.6080740697881435	.	pygwas	snp	ID=A_DBE1;name=1_7166126;pval=2.86551342293e-09;bonferroni_pval=0.0270782221340676;fdr=0.00058986193065;mafs=0.321646341463;macs=211;lnbf=11.629791777068874;ppa=0.6080740697881435;leadSNP=True;variant=nan;rank=nan;effect=nan;gene=nan
#1	7166142	7166143	1_7166143	0.0220654603628932	.	pygwas	snp	ID=A_DBE1;name=1_7166143;pval=8.30317965771e-08;bonferroni_pval=0.7846249868920459;fdr=0.00058986193065;mafs=0.397865853659;macs=261;lnbf=8.31350858365981;ppa=0.02206546036289321;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#
#
## Example -feat
#1	0	157	MYBrelated.TBP3_col.1	195	.	neomorph	peak
#1	0	161	MYBrelated.AT1G72740_col.1	182	.	neomorph	peak
#
## Example intersect
#1	790786	790787	1_790787	0.1576860343004641	.	pygwas	snp	ID=A_DBE1;name=1_790787;pval=9.76206703943e-09;bonferroni_pval=0.0922485365680324;fdr=0.00058986193065;mafs=0.289634146341;macs=190;lnbf=10.466598221666702;ppa=0.15768603430046416;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan	1	790605	790806	C3H.At5g08750_col.7	115	.	neomorph	peak
#1	790786	790787	1_790787	0.1576860343004641	.	pygwas	snp	ID=A_DBE1;name=1_790787;pval=9.76206703943e-09;bonferroni_pval=0.0922485365680324;fdr=0.00058986193065;mafs=0.289634146341;macs=190;lnbf=10.466598221666702;ppa=0.15768603430046416;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan	1	790730	790931	TCP.TCP24_col.29	114	.	neomorph	peak
