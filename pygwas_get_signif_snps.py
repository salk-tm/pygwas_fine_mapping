#!/usr/bin/env python3
#
#   usage:
#
#       mamba activate pygwas
#
#       pygwas_closest_to_signif.py <filename> <raw|bonferroni|bh> <alpha> <macsCutoff>
#
#   description:
#       Takes a CSV file output by pygwas convert and outputs a BED file for
#       SNPs with p-values smaller than alpha and MACs greater than macsCutoff.
#       If bonferroni is specified then the bonferroni adjusted p-value is
#       calculated based on number of snps in whole file and an alpha of alpha,
#       likewise, bh calculates FDR, and if raw is specified the value of alpha
#       is used directly. <distance> is the maximum distance from the lead snp
#       to search for other snps. <bfprocessors> is the number of parallel jobs
#       for running bayes factor/ppa calculations. snpVCFbgz must be the relative
#       path to the actual file, not a symlink.

import sys
import os
import pandas as pd
from statsmodels.stats.multitest import fdrcorrection

# Get params
filename = sys.argv[1]
method = sys.argv[2]
alpha = float(sys.argv[3])
macsthresh = int(sys.argv[4])

# Read csv
print("Reading pygwas csv file")
csvDf = pd.read_csv(filename, header = 0)

# Get prefix (gene name)
infilePrefix = os.path.basename(filename.rstrip(".csv"))

# Get #snps for calculating bonferroni adjusted p-values
# Get #snps for calculating bonferroni adjusted p-values
num_snps = csvDf.shape[0]

# Add fdr and bonferroni-adjusted p-values to table
print("Adding fdr column")
csvDf["fdr"] = fdrcorrection(csvDf["pvals"], alpha =  alpha)[1]

# Subset for significant snps
print("Subsetting significant snps")
if method == "bh":
    signifDf = csvDf[(csvDf["fdr"] < alpha) & (csvDf["macs"] > macsthresh)]
elif method == "bonferroni":
    signifDf = csvDf[((csvDf["pvals"] * num_snps) < alpha) & (csvDf["macs"] > macsthresh)]
else:
    signifDf = csvDf[(csvDf["pvals"] < alpha) & (csvDf["macs"] > macsthresh)]

# Write table for signif snps
print("Writing of signif snps")
signifDf.to_csv(f"{infilePrefix}_{method}.alpha_{alpha}.macs_{macsthresh}.tsv", sep = "\t", header = False, index = False)
