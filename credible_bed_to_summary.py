#!/usr/bin/env python3
#
#   usage:
#       credible_bed_to_summary.py <bedfile> <genesymboltable>
#
#   This script parses the bed files output by fine_map.py to a table with
#   a summary of each line - MISSENSE MUTATIONS ONLY
#
#   Reads in the Araport11 gene id/symbol/description/extradescription table
#
#   It is assumed that the first part of the input bedfile filename is like this:
#   A_geneSymbol_snpID... where geneSymbol is the gene symbol of the GWAS gene, such
#   as ABCG20. E.g. A_AAE16_5_3945077...


import sys
import pandas as pd


class bedAttr:
    """ A class to represent the semicolon delimited attributes field such as in
    GFF3 and BED files (usually the 9th field)
    """
    def __init__(self, attrField):
        self.attributes = {attr.split("=")[0]:attr.split("=")[1] for attr in attrField.split(";")}

# Set max field width to avoid truncation
pd.set_option("max_colwidth", 100000000)

# Read CL args
bedpath = sys.argv[1]
geneSymbolTable = sys.argv[2]

# Read gene-description table
geneTableDf = pd.read_csv(geneSymbolTable, sep = "\t", names = ["id", "symbol", "description", "extra_description"])

# Initialize output lines list with header
summaryLines = []
with open(bedpath) as inf:
    for line in inf:
        fields = line.strip().split("\t")
        attrDict = bedAttr(fields[8])
        if "missense" in attrDict.attributes["variant"]:
            rank = attrDict.attributes["rank"].split("|")[0]
            effect = attrDict.attributes["effect"].split("|")[0].split(".")[1]
            gene = attrDict.attributes["gene"].split("|")[0]
            snpid = attrDict.attributes["ID"]
            fdr = attrDict.attributes["fdr"]
            ppa = attrDict.attributes["ppa"]
            mafs = attrDict.attributes["mafs"]
            macs = attrDict.attributes["macs"]
            symbol = geneTableDf[geneTableDf["id"] == gene]["symbol"].to_string(index = False)
            description = geneTableDf[geneTableDf["id"] == gene]["description"].to_string(index = False)
            extra_description = geneTableDf[geneTableDf["id"] == gene]["extra_description"].to_string(index = False)

            inputGene_symbol = snpid.split("_")[1]
            inputGene_id = geneTableDf[geneTableDf["symbol"] == inputGene_symbol]["id"].to_string(index = False)
            inputGene_description = geneTableDf[geneTableDf["symbol"] == inputGene_symbol]["description"].to_string(index = False)
            inputGene_extra_description = geneTableDf[geneTableDf["symbol"] == inputGene_symbol]["extra_description"].to_string(index = False)
            
            # Add line to output lines list
            if summaryLines == []:
                # Add header
                summaryLines.append("snpid\tinput_gene_id\tinput_gene_symbol\tinput_gene_description\tinput_gene_extra_description\teffect\trank\tgene\tsymbol\tdescription\textra_description\tfdr\tppa\tmafs\tmacs")

            summaryLines.append(f"{snpid}\t{inputGene_id}\t{inputGene_symbol}\t{inputGene_description}\t{inputGene_extra_description}\t{effect}\t{rank}\t{gene}\t{symbol}\t{description}\t{extra_description}\t{fdr}\t{ppa}\t{mafs}\t{macs}")


# Output table
if not summaryLines == []:
    print("\n".join(summaryLines))

# Example input file name from which the gene name will be parsed
#A_AAE16_5_3945077_closest_10000_signifSNP_annot_with_other_feats_minPPA_0.01_99_credible_set.bed:

## Example gene symbol table
#AT1G01010	NAC001	NAC domain containing protein 1	NAC domain containing protein 1;(source:Araport11) protein_coding NAC DOMAIN CONTAINING PROTEIN 1 (NAC001) NAC DOMAIN CONTAINING PROTEIN 1 (ANAC001); (NTL10);NAC DOMAIN CONTAINING PROTEIN 1 (NAC001)
#
#
## Example input bed file
#4	6122152	6122153	4_6122153	0.0006799835859637	.	pygwas	snp	ID=A_4CL5_4_6122153;Name=4_6122153;pval=1.61826797972e-07;bonferroni_pval=1.5292135600084225;fdr=0.0001723640171335;mafs=0.0259146341463;macs=17;lnbf=8.792275255853534;ppa=0.0006799835859637141;leadSNP=False;variant=missense_variant|missense_variant|3_prime_UTR_variant;rank=15|15|14;effect=p.His1133Tyr|p.His1133Tyr|.;gene=AT4G09680|AT4G09680|AT4G09680;otherFeature=BBRBPC.BPC1_colamp.10544,NAC.SND3_colamp.15933
#4	6122158	6122159	4_6122159	0.0006799835859637	.	pygwas	snp	ID=A_4CL5_4_6122159;Name=4_6122159;pval=1.61826797972e-07;bonferroni_pval=1.5292135600084225;fdr=0.0001723640171335;mafs=0.0259146341463;macs=17;lnbf=8.792275255853534;ppa=0.0006799835859637141;leadSNP=False;variant=missense_variant|missense_variant|3_prime_UTR_variant;rank=15|15|14;effect=p.Glu1135Lys|p.Glu1135Lys|.;gene=AT4G09680|AT4G09680|AT4G09680;otherFeature=NAC.SND3_colamp.15933
#4	6122158	6122159	4_6122159	0.0006856196265342	.	pygwas	snp	ID=A_4CL5_4_6122159;Name=4_6122159;pval=1.61826797972e-07;bonferroni_pval=1.5292135600084225;fdr=0.0001723640171335;mafs=0.0259146341463;macs=17;lnbf=8.792275255853534;ppa=0.0006856196265342498;leadSNP=False;variant=missense_variant|missense_variant|3_prime_UTR_variant;rank=15|15|14;effect=p.Glu1135Lys|p.Glu1135Lys|.;gene=AT4G09680|AT4G09680|AT4G09680;otherFeature=NAC.SND3_colamp.15933
#4	6122152	6122153	4_6122153	0.0006856196265342	.	pygwas	snp	ID=A_4CL5_4_6122153;Name=4_6122153;pval=1.61826797972e-07;bonferroni_pval=1.5292135600084225;fdr=0.0001723640171335;mafs=0.0259146341463;macs=17;lnbf=8.792275255853534;ppa=0.0006856196265342498;leadSNP=False;variant=missense_variant|missense_variant|3_prime_UTR_variant;rank=15|15|14;effect=p.His1133Tyr|p.His1133Tyr|.;gene=AT4G09680|AT4G09680|AT4G09680;otherFeature=BBRBPC.BPC1_colamp.10544,NAC.SND3_colamp.15933
#4	8116576	8116577	4_8116577	0.0844546495153591	.	pygwas	snp	ID=A_AAE15_4_8116577;Name=4_8116577;pval=1.14790850267e-08;bonferroni_pval=0.1084738294232117;fdr=0.023553323880248;mafs=0.123475609756;macs=81;lnbf=10.630228794450291;ppa=0.0844546495153591;leadSNP=False;variant=missense_variant;rank=3;effect=p.Ala150Val;gene=AT4G14070;otherFeature=none
#4	8116576	8116577	4_8116577	0.084454883972957	.	pygwas	snp	ID=A_AAE15_4_8116577;Name=4_8116577;pval=1.14790850267e-08;bonferroni_pval=0.1084738294232117;fdr=0.023553323880248;mafs=0.123475609756;macs=81;lnbf=10.630228794450291;ppa=0.084454883972957;leadSNP=False;variant=missense_variant;rank=3;effect=p.Ala150Val;gene=AT4G14070;otherFeature=none
#5	3240719	3240720	5_3240720	0.4970559568659665	.	pygwas	snp	ID=A_AAE16_5_3240720;Name=5_3240720;pval=4.19099364278e-15;bonferroni_pval=3.9603603289222667e-08;fdr=1.165154553963597e-11;mafs=0.0609756097561;macs=40;lnbf=25.465522765889858;ppa=0.4970559568659665;leadSNP=False;variant=missense_variant|3_prime_UTR_variant;rank=3|2;effect=p.Met258Arg|.;gene=AT5G10300|AT5G10300;otherFeature=WRKY.WRKY15_col.18337,WRKY.WRKY75_col.15628,WRKY.WRKY8_col.16569,C2C2dof.At1g64620_colamp.17049,C3H.AT5G63260_col.11438,C2C2dof.AT2G28810_col.19223,C2C2dof.AT2G28810_colamp.20496,C2C2dof.OBP3_col.13274,Orphan.BBX31_col.13424,C2C2dof.At5g62940_col.23869,C2C2dof.AT5G66940_col.19136,C2C2dof.dof24_col.15391,C2C2dof.OBP1_col.18879,LOBAS2.LBD19_colamp.12359,RAV.RAV1_col.9535
#5	3240719	3240720	5_3240720	0.4970559568727775	.	pygwas	snp	ID=A_AAE16_5_3240720;Name=5_3240720;pval=4.19099364278e-15;bonferroni_pval=3.9603603289222667e-08;fdr=1.165154553963597e-11;mafs=0.0609756097561;macs=40;lnbf=25.465522765889858;ppa=0.49705595687277754;leadSNP=True;variant=missense_variant|3_prime_UTR_variant;rank=3|2;effect=p.Met258Arg|.;gene=AT5G10300|AT5G10300;otherFeature=WRKY.WRKY15_col.18337,WRKY.WRKY75_col.15628,WRKY.WRKY8_col.16569,C2C2dof.At1g64620_colamp.17049,C3H.AT5G63260_col.11438,C2C2dof.AT2G28810_col.19223,C2C2dof.AT2G28810_colamp.20496,C2C2dof.OBP3_col.13274,Orphan.BBX31_col.13424,C2C2dof.At5g62940_col.23869,C2C2dof.AT5G66940_col.19136,C2C2dof.dof24_col.15391,C2C2dof.OBP1_col.18879,LOBAS2.LBD19_colamp.12359,RAV.RAV1_col.9535
#5	3954380	3954381	5_3954381	0.1664657074534841	.	pygwas	snp	ID=A_AAE16_5_3954381;Name=5_3954381;pval=4.07214502064e-15;bonferroni_pval=3.8480520296526665e-08;fdr=1.133113082936592e-11;mafs=0.0411585365854;macs=27;lnbf=25.67959719802256;ppa=0.16646570745348413;leadSNP=False;variant=missense_variant;rank=4;effect=p.His183Gln;gene=AT5G12230;otherFeature=MYB.MYB116_colamp.7230,C2H2.SGR5_colamp.13546,C2H2.At5g66730_colamp.4583,C2H2.IDD5_colamp.7385,MYBrelated.At1g49010_colamp.6563,MYBrelated.At5g05790_col.2903,MYBrelated.At1g49010_col.16601,MYBrelated.At5g58900_colamp.15277
#5	3954380	3954381	5_3954381	0.1409228005970811	.	pygwas	snp	ID=A_AAE16_5_3954381;Name=5_3954381;pval=4.07214502064e-15;bonferroni_pval=3.8480520296526665e-08;fdr=1.133113082936592e-11;mafs=0.0411585365854;macs=27;lnbf=25.67959719802256;ppa=0.14092280059708118;leadSNP=False;variant=missense_variant;rank=4;effect=p.His183Gln;gene=AT5G12230;otherFeature=MYB.MYB116_colamp.7230,C2H2.SGR5_colamp.13546,C2H2.At5g66730_colamp.4583,C2H2.IDD5_colamp.7385,MYBrelated.At1g49010_colamp.6563,MYBrelated.At5g05790_col.2903,MYBrelated.At1g49010_col.16601,MYBrelated.At5g58900_colamp.15277
