#!/usr/bin/env python3
#
#   usage:
#       bed_ppa_to_credible_set.py -bed <bed> [-csThresh <float> -minPPA <float>]
#
#   description:
#       input BED file should be sorted by PPA (column 5). Outputs BED 
#       lines within <csThresh> (percent. e.g. 95) credible set. Default credible
#       set is 98%. Default minPPA = 0.1%
#       
import sys
from subprocess import run

args = sys.argv

bedpath = args[args.index("-bed") + 1]
if "-csThresh" in args:
    cs_thresh = float(args[args.index("-csThresh") + 1]) / 100
else:
    cs_thresh = 0.98

if "-minPPA" in args:
    minppa = float(args[args.index("-minPPA") + 1])
else:
    minppa = 0.001

credible_set_lines = list()
ppalist = list()
with open(bedpath) as inf:
    for line in inf:
        linesplit = line.strip().split("\t")
        ppa = float(linesplit[4])
        #if ppalist == []:
        #    if ppa > minppa:
        #        ppalist.append(ppa)
        #        credible_set_lines.append(line.strip())
        #    continue
        if not ppalist == [] and not ppa <= ppalist[-1]:
            exit("Input file not sorted by PPA (column 5)")
        if sum(ppalist) > cs_thresh or ppa < minppa:
            print("\n".join(credible_set_lines))
            break
        ppalist.append(ppa)
        credible_set_lines.append(line.strip())
        
        
## Example input
#1	22125542	22125543	1_22125543	0.19995099997716528	.	pygwas	snp	ID=1_22125543;pval=2.56066810641e-09;bonferroni_pval=0.0241975274804658;fdr=0.0001766242881785;mafs=0.0289634146341;macs=19;lnbf=12.762943379246224;ppa=0.19995099997716528;inBlock=False;leadSNP=True;variant=nan;rank=nan;effect=nan;gene=nan
#1	22125561	22125562	1_22125562	0.19995099997716528	.	pygwas	snp	ID=1_22125562;pval=2.56066810641e-09;bonferroni_pval=0.0241975274804658;fdr=0.0001766242881785;mafs=0.0289634146341;macs=19;lnbf=12.762943379246224;ppa=0.19995099997716528;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#1	22125563	22125564	1_22125564	0.19995099997716528	.	pygwas	snp	ID=1_22125564;pval=2.56066810641e-09;bonferroni_pval=0.0241975274804658;fdr=0.0001766242881785;mafs=0.0289634146341;macs=19;lnbf=12.762943379246224;ppa=0.19995099997716528;inBlock=False;leadSNP=False;variant=intergenic_region;rank=-1;effect=.;gene=AT1G60050-AT1G08297
#1	22125567	22125568	1_22125568	0.19995099997716528	.	pygwas	snp	ID=1_22125568;pval=2.56066810641e-09;bonferroni_pval=0.0241975274804658;fdr=0.0001766242881785;mafs=0.0289634146341;macs=19;lnbf=12.762943379246224;ppa=0.19995099997716528;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#1	22125610	22125611	1_22125611	0.19995099997716528	.	pygwas	snp	ID=1_22125611;pval=2.56066810641e-09;bonferroni_pval=0.0241975274804658;fdr=0.0001766242881785;mafs=0.0289634146341;macs=19;lnbf=12.762943379246224;ppa=0.19995099997716528;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#1	22126838	22126839	1_22126839	6.563577152304324e-05	.	pygwas	snp	ID=1_22126839;pval=4.74893804119e-05;bonferroni_pval=448.7600656526685;fdr=0.0223225197535562;mafs=0.0015243902439;macs=1;lnbf=4.74123660878435;ppa=6.563577152304324e-05;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#1	22140326	22140327	1_22140327	6.563577152304324e-05	.	pygwas	snp	ID=1_22140327;pval=4.74893804119e-05;bonferroni_pval=448.7600656526685;fdr=0.0223225197535562;mafs=0.0015243902439;macs=1;lnbf=4.74123660878435;ppa=6.563577152304324e-05;inBlock=False;leadSNP=False;variant=intron_variant|intron_variant;rank=3|3;effect=.|.;gene=AT1G60060|AT1G60060
#1	22107638	22107639	1_22107639	1.0084357482921644e-05	.	pygwas	snp	ID=1_22107639;pval=0.000167074897577;bonferroni_pval=1578.8064901090938;fdr=0.0386583371721129;mafs=0.00609756097561;macs=4;lnbf=2.8681012230528706;ppa=1.0084357482921644e-05;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#1	22134451	22134452	1_22134452	5.681199506132674e-06	.	pygwas	snp	ID=1_22134452;pval=0.000642848417615;bonferroni_pval=6074.720191997542;fdr=0.0896308937033883;mafs=0.0015243902439;macs=1;lnbf=2.2942781549423796;ppa=5.681199506132674e-06;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
#1	22112818	22112819	1_22112819	5.334384876257773e-06	.	pygwas	snp	ID=1_22112819;pval=0.000292716176793;bonferroni_pval=2766.0780068275744;fdr=0.063089088742532;mafs=0.00762195121951;macs=5;lnbf=2.2312893422833326;ppa=5.334384876257773e-06;inBlock=False;leadSNP=False;variant=nan;rank=nan;effect=nan;gene=nan
