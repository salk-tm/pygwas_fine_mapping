#!/usr/bin/env python3
#
#   usage:
#       credset2summary.py -dir <cred_set_dir> -out <out_file> [-target <at_locus_id>]
#
#   description:
#       From a directory with BED files created by the finemap.py pipeline as
#       well as accessory files, create a table with the following information:
#       - SNP coordinates
#       - GWAS statistics
#       - SNP annotation
#           - Gene body effect
#           - Transcription factor binding site
#           - Binding motif in TFBS
#
#       If -target is given, then each TF (Overlapping SNPs, TFBS overlapping
#       SNPs, or genes to the left and right of SNPs) will be reported as 
#       having bound its promoter in DAP-seq and ampDAP-seq experiments, or not.
#       If -target is not given, the input file names will be parsed to obtain
#       the gene: <gene_symbol>_<chr>_<position>.
#
#       Note: the script obtains the target gene from the first file, assuming
#             all files are intended for use with the same target.


import sys
import pandas as pd
import numpy as np
import subprocess as sp
import os
import re
from collections import OrderedDict as odict
from shutil import rmtree, copyfile


class bedAttr:
    """ A class to represent the semicolon delimited attributes field such as in
    GFF3 and BED files (usually the 9th field)
    """
    def __init__(self, attrField):
        self.attributes = {attr.split("=")[0]:attr.split("=")[1] for attr in attrField.split(";")}


def getArg(args, default = None, silent = False):
    '''
    Returns the item one index further than the item matching an arg in the 
    list sys.argv. If it doesn't exists return default, and if there is no
    default, it returns a message saying there is no default. A list of
    multiple arguments may be provided at once, and the first match is
    returned. If silent == True, no warnings are printed if the argument is not
    found.
    '''

    # Convert string input to list
    if isinstance(args, str):
        args = [args]

    # Copy args for reporting if fails with not default
    inputArgs = args.copy()

    # Try to find args
    while len(args) > 0:
        try:
            argRes = sys.argv[sys.argv.index(args[0]) + 1]
        except:
            argRes = False
            del(args[0])
        else:
            break

    # If args not found, set default or report no default available
    if not argRes:
        if default == None:
            if silent:
                exit()
            else:
                exit(f"No default for: {inputArgs}")
        else:
            argRes = default

    # Return the arg value
    return(argRes)

def keyvalstr2list(keyvalstr, keyvalsep, pairsep):
    """
    Converts a string containing key value pairs key<keyvalsep>value
    separated by <pairsep> into an ordered dict.
    """
    keyvalList = [pair.split(keyvalsep) for pair in keyvalstr.split(pairsep)]

    return(keyvalList)

def twocol2dict(filepath, sep, side = "left"):
    """
    Reads in a two-column sep-delimited file and creates key:value pairs for
    each field in the first, second, or both columns. Keys are strings and 
    values are sets, making them nonredundant:
        left    {field1:field2 for all rows}
        right   {field2:field1 for all rows}
        both    {field1:field2, field2:field11 for all rows}
    """
    tableDict = dict()
    with open(filepath) as inf:
        for line in inf:
            field1, field2 = line.strip().split(sep)
            if side in ["left", "both"]:
                if field1 in tableDict:
                    tableDict[field1].add(field2)
                else:
                    tableDict[field1] = set([field2])
            if side in ["right", "both"]:
                if field2 in tableDict:
                    tableDict[field2].add(field1)
                else:
                    tableDict[field2] = set([field1])

    return tableDict


# Read command line arguments
if len(set(["-d", "-dir", "-o", "-out"]).intersection(sys.argv[1:])) == 0:
    print("""
   usage:
       credset2summary.py -dir <cred_set_dir> -out <out_file> [-target <at_locus_id>]

   description:
       From a directory with BED files created by the finemap.py pipeline as
       well as accessory files, create a table with the following information:
       - SNP coordinates
       - GWAS statistics
       - SNP annotation
           - Gene body effect
           - Transcription factor binding site
           - Binding motif in TFBS

       If -target is given, then each TF (Overlapping SNPs, TFBS overlapping
       SNPs, or genes to the left and right of SNPs) will be reported as 
       having bound its promoter in DAP-seq and ampDAP-seq experiments, or not.
       If -target is not given, the input file names will be parsed to obtain
       the gene: <gene_symbol>_<chr>_<position>.

       Note: the script obtains the target gene from the first file, assuming
             all files are intended for use with the same target.

       Example:
        credset2summary.py -d CLSY1_finemap/credible_sets -o CLSY4_finemapsummary.tsv
        credset2summary.py -d CLSY1_finemap/credible_sets -o CLSY4_finemapsummary.tsv -target AT3G42670
""", file = sys.stderr)


# Setup #

## Command line arguments
indir = getArg(["-d", "-dir"])
outfile = getArg(["-o", "-out"])
## If -target is provided, sets the gene of interest (goi) to assess for upstream tfs
if "-target" in sys.argv or "-t" in sys.argv:
    goi = getArg(["-t", "-dir"], silent = True)
else:
    goi = None
## Executing script directory, in which should be the database files
basedir = os.path.dirname(os.path.realpath(__file__))
## Make directory for temporary files such as for use with bedtools
tmpdir = f"{basedir}/finemap2summary.tmp"
os.makedirs(tmpdir, exist_ok = True)
## Directory with data files: motif and gene loci, gene annotations
dbdir = f"{basedir}/dbs"


# External data files #

## Reference genome sequence
refseq = f"{dbdir}/athaliana_genome_assembly.HPIv02.fasta"
## Gene locus annotations
genelocifile = f"{dbdir}/Athaliana.Col-0.HPIv01.gene.genes.bed"
## Gene annotations (locus_id, symbol, descripiton, long_description)
annotfile = f"{dbdir}/Araport11_geneId_to_symbol.tsv"
## Create map for gene annotations: locus_id:[gene_symbol, description, long_description], and same for gene symbol
annotLocusDict = {fields[0]:fields[1:] for fields in [line.split("\t") for line in open(annotfile).read().strip().split("\n")]}
annotSymbolDict = {fields[1]:[fields[0], fields[2], fields[3]] for fields in [line.split("\t") for line in open(annotfile).read().strip().split("\n")]}
## Cistrome motif annotations used with bedtools
motiffile = f"{dbdir}/cistrome_motifs_fimo_qval_0.05.bed"
## Map of cistrome TF names to locus ids
cistromenamefile = f"{dbdir}/cistrome_name_locus_id.tsv"
## Create map of cistrome tf names to locus ids
cistromeNamesDict = {names[0]:names[1] for names in [line.split("\t") for line in open(cistromenamefile).read().strip().split("\n")]}
## ampDAP-seq tf:target map
ampdapTargetDict = twocol2dict(filepath = f"{dbdir}/cistrome_targets.ampdap.tsv", sep = "\t", side = "left")
## DAP-seq tf:target map
dapTargetDict = twocol2dict(filepath = f"{dbdir}/cistrome_targets.dap.tsv", sep = "\t", side = "left")
## Protein protein interactions map
ppiDict = twocol2dict(filepath = f"{dbdir}/ppi.attfin_string.tsv", sep = "\t", side = "both")


# Input files #

## Read each credible set BED file into data frame
files = os.listdir(indir)
credsetfiles = (indir + "/" + f for f in files if f.endswith(".bed"))
## Obtain gene of interest from filename. Try the first and second substring between underscores as will be the default naming from finemap.py
### Example filename: CLSYs_CLSY4_2_4198702_closest_4000_signifSNP_annot_with_other_feats_minPPA_.05_99_credible_set.bed
if not goi:
    gene = os.path.basename(files[0]).split("_")[0]
    if gene not in annotSymbolDict and gene not in annotLocusDict:
        gene = os.path.basename(files[0]).split("_")[1]
        if gene not in annotSymbolDict and gene not in annotLocusDict:
            exit("Gene id not obtained from credible set file names. Pass the target explicitly with -target.")
    try:
        goi = annotSymbolDict[gene][0]
    # It is already the locus id
    except ValueError:
        pass


# Construct summary table #

## Column names for parsed credible set bed files
colnames = ["chr", "start", "end", "id", "stat", "strand", "source", "type", "attr"]
## Read all credible set bed files and concatenate into a single data frame, and drop stat column which is a duplicate of ppa in the attr column
credsetsDf = pd.concat((pd.read_csv(f, sep = "\t", names = colnames) for f in credsetfiles), ignore_index = True, copy = False, axis = 0).drop("stat", axis = 1)
## Parse the attributes column
attrDf = pd.DataFrame([odict(keyvalstr2list(item, "=", ";")) for item in credsetsDf["attr"]])
## Remove the original attributes column
credsetsDf.drop("attr", inplace = True, axis = 1)
## Add the attributes data frame, remove lead snp column, and keep only unique snps (from any credible set)
credsetsDf = pd.concat((credsetsDf, attrDf), axis = 1).drop(["strand", "source", "type", "ID", "leadSNP"], axis = 1).drop_duplicates(subset = ["chr", "start", "end"])
## Add column for gene of interest
credsetsDf.insert(loc = 0, column = "target_gene_symbol", value = annotLocusDict[goi][0])
credsetsDf.insert(loc = 0, column = "target_locus_id", value = goi)
## Replace nan with . and rename gene column variant_gene
credsetsDf = credsetsDf.replace({"nan":"."}).rename(columns = {"gene":"variant_locus_id", "rank":"exon_intron_num"})
## Remove redundancies in cis hit columns (snpeff variant type and gene may have repeated values pipe delimited)
credsetsDf.loc[:, "variant"] = [",".join(list(set(variant.split("|")))) for variant in credsetsDf.loc[:, "variant"]]
credsetsDf.loc[:, "exon_intron_num"] = [",".join(list(set(variant.split("|")))) for variant in credsetsDf.loc[:, "exon_intron_num"]]
credsetsDf.loc[:, "effect"] = [",".join(list(set(variant.split("|")))) for variant in credsetsDf.loc[:, "effect"]]
credsetsDf.loc[:, "variant_locus_id"] = [list(set(variant.split("|")))[0] for variant in credsetsDf.loc[:, "variant_locus_id"]]


# Genes to the left and right of each snp #

## Write BED file for snps
snpbedTempfile = f"{tmpdir}/unique_snps.bed"
## Sort data frame for bedtools input
credsetsDf.sort_values(by = ["chr", "start"], inplace = True)
credsetsDf.loc[:, ["chr", "start", "end"]].to_csv(snpbedTempfile, sep = "\t", header = False, index = False)
## BED closest - each snp will have the same closest genes
closestgenesbed = f"{tmpdir}/snpclosestgene.bed"
sp.run([f"{basedir}/closest_genes.sh", snpbedTempfile, genelocifile, closestgenesbed, tmpdir])

## Add BED closest results: genes and distance between their TSS and each snp
### New columns: left_locus_id, snp_to_left_gene, right_locus_id, snp_to_right_gene
closestGenesDf = pd.read_csv(closestgenesbed, header = 0, sep = "\t")
credsetsDf.index = list(range(len(credsetsDf.index)))
credsetsDf = pd.concat((credsetsDf, closestGenesDf), axis = 1)
#credsetsDf = pd.concat((credsetsDf, closestGenesDf), axis = 1, ignore_index = True)


# Expand TFBS column which may contain multiple TFBS #

## Split TFBS attr string into list
credsetsDf.loc[:, "otherFeature"] = pd.Series([tfbs.split(",") for tfbs in credsetsDf.loc[:, "otherFeature"]])
credsetsDf = credsetsDf.explode("otherFeature")
## Write bed files with lines for each tfbs
tfbsTempfile = f"{tmpdir}/tfbsfile.tsv"
credsetsDf.loc[:, ["chr", "start", "end", "otherFeature"]].to_csv(tfbsTempfile, sep = "\t", header = False, index = False)
## Split tf+cistrome  and translate cistrome tf names
tfbsList = [[tfbs.split(".")[0]] + tfbs.split(".")[1].split("_") if tfbs.split(".")[0] != "none" else ["none"] * 3 for tfbs in credsetsDf.loc[:, "otherFeature"]]
tfbsList = [[tfdat[0], cistromeNamesDict[tfdat[1]], annotLocusDict[cistromeNamesDict[tfdat[1]]][0], annotLocusDict[cistromeNamesDict[tfdat[1]]][2], tfdat[2]] if tfdat[0] != "none" else ["none"] * 4 for tfdat in tfbsList]
## Create data frame for the information
tfbsDf = pd.DataFrame(tfbsList, columns = ["tfbs_tf_family", "tfbs_tf_locus_id", "tfbs_tf_gene_symbol", "tfbs_tf_gene_description", "experiment_type"])
## Convert cistrome experiment labels into experiment types (col and colamp to dap and ampdap)
tfbsDf.loc[:, "experiment_type"] = ["DAP" if label == "col" else "ampDAP" if label == "colamp" else "none" for label in tfbsDf.loc[:, "experiment_type"]]
## Add tfbs columns
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "experiment_type", value = tfbsDf.loc[:, "experiment_type"].tolist())
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_tf_family", value = tfbsDf.loc[:, "tfbs_tf_family"].tolist())
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_tf_locus_id", value = tfbsDf.loc[:, "tfbs_tf_locus_id"].tolist())
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_tf_gene_symbol", value = tfbsDf.loc[:, "tfbs_tf_gene_symbol"].tolist())
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_tf_gene_description", value = tfbsDf.loc[:, "tfbs_tf_gene_description"].tolist())


# Add motif info if snp in tfbs motif #

## Intersect snps with motifs
snpMotifTempFile = f"{tmpdir}/snpMotif.bed"
sp.run([f"{basedir}/intersect_motifs.sh", tfbsTempfile, motiffile, refseq, snpMotifTempFile, tmpdir])
## Add columns with default value of .
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "in_tfbs_motif", value = ".")
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_motif_len", value = ".")
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_motif_snp_position", value = ".")
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id"), column = "tfbs_motif_seq", value = ".")
## Remove trailing number from cistrome tf names
credsetsDf["otherFeature"] = [re.sub("\.[0-9]+", "", tf) for tf in credsetsDf.loc[:, "otherFeature"]]
## Read snps x motif intersect bed
snpMotifDf = pd.read_csv(snpMotifTempFile, sep = "\t", header = 0)
## Insert data for each snp in a motif of the tfbs it is in
for indx in snpMotifDf.index:
    snpMotifRows = (credsetsDf["otherFeature"] == snpMotifDf.at[indx, "other_tfbs_motif"]) & (credsetsDf["chr"] == snpMotifDf.at[indx, "snp_chr"]) & (credsetsDf["start"] == snpMotifDf.at[indx, "snp_start"])
    credsetsDf.loc[snpMotifRows, "in_tfbs_motif"] = "yes"
    credsetsDf.loc[snpMotifRows, "tfbs_motif_len"] = snpMotifDf.at[indx, "motif_len"]
    credsetsDf.loc[snpMotifRows, "tfbs_motif_snp_position"] = snpMotifDf.at[indx, "snp_pos_relative_to_motif_start"]
    credsetsDf.loc[snpMotifRows, "tfbs_motif_seq"] = snpMotifDf.at[indx, "motif_seq"]
## Remove cistrome tfbs label
credsetsDf = credsetsDf.drop("otherFeature", axis = 1)


# Add gene symbols and descriptions for variant, left, and right gene #

## Variant
credsetsDf.insert(loc = credsetsDf.columns.get_loc("variant_locus_id") + 1, column = "variant_gene_description", value = ["." if gene not in annotLocusDict else annotLocusDict[gene][2] for gene in credsetsDf.loc[:, "variant_locus_id"]])
credsetsDf.insert(loc = credsetsDf.columns.get_loc("variant_locus_id") + 1, column = "variant_gene_symbol", value = ["." if gene not in annotLocusDict else annotLocusDict[gene][0] for gene in credsetsDf.loc[:, "variant_locus_id"]])
## Left
leftDescriptions = [annotLocusDict[gene][2] if gene in annotLocusDict else "." for gene in credsetsDf.loc[:, "left_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id") + 1, column = "left_gene_description", value = leftDescriptions)
leftSymbols = [annotLocusDict[gene][0] if gene in annotLocusDict else "." for gene in credsetsDf.loc[:, "left_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("left_locus_id") + 1, column = "left_gene_symbol", value = leftSymbols)
## Right
rightDescriptions = [annotLocusDict[gene][2] if gene in annotLocusDict else "." for gene in credsetsDf.loc[:, "right_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("right_locus_id") + 1, column = "right_gene_description", value = rightDescriptions)
rightSymbols = [annotLocusDict[gene][0] if gene in annotLocusDict else "." for gene in credsetsDf.loc[:, "right_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("right_locus_id") + 1, column = "right_gene_symbol", value = rightSymbols)


# Is the gene of interest a target of another gene? #

## Variant gene
rightCol = credsetsDf.columns.get_loc("variant_gene_description") + 1
credsetsDf.insert(loc = rightCol, column = f"variant_gene_ampdap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in ampdapTargetDict else "true" if goi in ampdapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "variant_locus_id"]])
credsetsDf.insert(loc = rightCol, column = f"variant_gene_dap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in dapTargetDict else "true" if goi in dapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "variant_locus_id"]])
## SNP TFBS
tfCol = credsetsDf.columns.get_loc("tfbs_motif_seq") + 1
credsetsDf.insert(loc = tfCol, column = f"tfbs_tf_ampdap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in ampdapTargetDict else "true" if goi in ampdapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "tfbs_tf_locus_id"]])
credsetsDf.insert(loc = tfCol, column = f"tfbs_tf_dap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in dapTargetDict else "true" if goi in dapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "tfbs_tf_locus_id"]])
## Gene left of snp
leftCol = credsetsDf.columns.get_loc("snp_to_left_gene") + 1
credsetsDf.insert(loc = leftCol, column = f"left_gene_ampdap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in ampdapTargetDict else "true" if goi in ampdapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "left_locus_id"]])
credsetsDf.insert(loc = leftCol, column = f"left_gene_dap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in dapTargetDict else "true" if goi in dapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "left_locus_id"]])
## Gene right of snp
rightCol = credsetsDf.columns.get_loc("snp_to_right_gene") + 1
credsetsDf.insert(loc = rightCol, column = f"right_gene_ampdap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in ampdapTargetDict else "true" if goi in ampdapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "right_locus_id"]])
credsetsDf.insert(loc = rightCol, column = f"right_gene_dap_binds_{annotLocusDict[goi][0]}", value = ["." if gene not in dapTargetDict else "true" if goi in dapTargetDict[gene] else "false" for gene in credsetsDf.loc[:, "right_locus_id"]])


# Columns for PPIs between any gene in the table

## Nonredundant list of genes in the table
allgenes = list(set(credsetsDf.loc[:, "left_locus_id"].tolist() + credsetsDf.loc[:, "right_locus_id"].tolist() + credsetsDf.loc[:, "tfbs_tf_locus_id"].tolist() + credsetsDf.loc[:, "variant_locus_id"].tolist()))
if "." in allgenes:
    del allgenes[allgenes.index(".")]
## Dictionary of table genes to interactors
interactorLocusIdDict = dict()
interactorSymbolDict = dict()
for gene in sorted(list(allgenes)):
    if gene in ppiDict:
        interactorsLocusIds = [interactor for interactor in allgenes if interactor in ppiDict[gene]]
        interactorsSymbols = [annotLocusDict[gene][0] for gene in interactorsLocusIds]
        interactorsLocusIds = ["."] if len(interactorsLocusIds) == 0 else interactorsLocusIds
        interactorsSymbols = ["."] if len(interactorsSymbols) == 0 else interactorsSymbols
        interactorLocusIdDict[gene] = ",".join(interactorsLocusIds)
        interactorSymbolDict[gene] = ",".join(interactorsSymbols)

## Gene variant (cis snp hits)
variantLocusIdPPIs = [interactorLocusIdDict[gene] if gene in interactorLocusIdDict else "."  for gene in credsetsDf.loc[:, "variant_locus_id"]]
variantSymbolPPIs = [interactorSymbolDict[gene] if gene in interactorSymbolDict else "."  for gene in credsetsDf.loc[:, "variant_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("variant_gene_description") + 1, column = "variant_ppis_gene_symbol", value = variantSymbolPPIs)
credsetsDf.insert(loc = credsetsDf.columns.get_loc("variant_gene_description") + 1, column = "variant_ppis_locus_id", value = variantLocusIdPPIs)
## Genes to the left of snp
leftLocusIdPPIs = [interactorLocusIdDict[gene] if gene in interactorLocusIdDict else "."  for gene in credsetsDf.loc[:, "left_locus_id"]]
leftSymbolPPIs = [interactorSymbolDict[gene] if gene in interactorSymbolDict else "."  for gene in credsetsDf.loc[:, "left_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("snp_to_left_gene") + 1, column = "left_ppis_gene_symbol", value = leftSymbolPPIs)
credsetsDf.insert(loc = credsetsDf.columns.get_loc("snp_to_left_gene") + 1, column = "left_ppis_locus_id", value = leftLocusIdPPIs)
## Genes to the right of snp
rightLocusIdPPIs = [interactorLocusIdDict[gene] if gene in interactorLocusIdDict else "."  for gene in credsetsDf.loc[:, "right_locus_id"]]
rightSymbolPPIs = [interactorSymbolDict[gene] if gene in interactorSymbolDict else "."  for gene in credsetsDf.loc[:, "right_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("snp_to_right_gene") + 1, column = "right_ppis_gene_symbol", value = rightSymbolPPIs)
credsetsDf.insert(loc = credsetsDf.columns.get_loc("snp_to_right_gene") + 1, column = "right_ppis_locus_id", value = rightLocusIdPPIs)
## TFBS TFs
tfLocusIdPPIs = [interactorLocusIdDict[gene] if gene in interactorLocusIdDict else "."  for gene in credsetsDf.loc[:, "tfbs_tf_locus_id"]]
tfSymbolPPIs = [interactorSymbolDict[gene] if gene in interactorSymbolDict else "."  for gene in credsetsDf.loc[:, "tfbs_tf_locus_id"]]
credsetsDf.insert(loc = credsetsDf.columns.get_loc("tfbs_motif_seq") + 1, column = "tfbs_tf_ppis_gene_symbol", value = tfSymbolPPIs)
credsetsDf.insert(loc = credsetsDf.columns.get_loc("tfbs_motif_seq") + 1, column = "tfbs_tf_ppis_locus_id", value = tfLocusIdPPIs)


# Output #

# Write table
credsetsDf.to_csv(outfile, sep = "\t", header = True, index = False)


# Cleanup #

# Remove temp directory
rmtree(tmpdir)
