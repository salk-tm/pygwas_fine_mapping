#!/usr/bin/bash
#
#	usage:
#       closest_genes.sh <snps> <genes.bed> <outfile> <tempdir>
#
#   description:
#       All inputs and output are bed files.
#       Finds genes to the left and right of features in snps and writes a bed
#       file with fields: chr, start, end, left gene, distance, right gene, distance
#       Distances are relative to the orientation of the gene: - means upstream 
#       of the TSS, + means downstream of the TTS.

SNPS=$1
BED=$2
OUT=$3
TMPDIR=$4

# Sort the snps bed for bedtools - this should be done by credset2summary.py to avoid mismatching rows
#sort -k1,1 -k2,2n $SNPS -o $SNPS

#echo "Gene to the right: (+) * (strand) * (-1)"
#bedtools closest -iu -D ref -a $SNPS -b $BED | cut -f1-3,7,9,14 | sed 's/\t-\t/\t-1\t/' | sed 's/\t+\t/\t1\t/'
#echo "Gene to the left: (+) * (strand) * (-1)"
#bedtools closest -id -D ref -a $SNPS -b $BED | cut -f1-3,7,9,14 | sed 's/\t-\t/\t-1\t/' | sed 's/\t+\t/\t1\t/'

# Finds nearest gene on the left (smaller coordinates)
bedtools closest -id -D ref -a $SNPS -b $BED | cut -f1-3,7,9,14 | sed 's/\t-\t/\t-1\t/' | sed 's/\t+\t/\t1\t/' | cut -f4-6 > ${TMPDIR}/left.tmp
# Finds nearest gene on the right (larger coordinates)
bedtools closest -iu -D ref -a $SNPS -b $BED | cut -f1-3,7,9,14 | sed 's/\t-\t/\t-1\t/' | sed 's/\t+\t/\t1\t/' | cut -f4-6 > ${TMPDIR}/right.tmp
# Calculates correct direction relative to each gene and concatenates results
## Add header
#echo -e "snp_chr\tsnp_start\tsnp_end\tleft_gene\tsnp_to_left_gene\tright_gene\tsnp_to_right_gene" > $OUT
echo -e "left_locus_id\tsnp_to_left_gene\tright_locus_id\tsnp_to_right_gene" > $OUT
#paste left.tmp right.tmp | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5*$6*-1"\t"$7"\t"$8*$9*-1}' >> $OUT
paste ${TMPDIR}/left.tmp ${TMPDIR}/right.tmp | awk '{print $1"\t"$2*$3*-1"\t"$4"\t"$5*$6*-1}' >> $OUT
# Removes intermediate files
rm ${TMPDIR}/left.tmp ${TMPDIR}/right.tmp
