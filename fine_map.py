#!/usr/bin/env python3
#
#   usage:
#
#       mamba activate pygwas
#
#       fine_map.py <filename> <raw|bonferroni|bh> <alpha> <macsCutoff> <distance> <snpVCFbgz> <processes> <bfprocesses> <featfile> <crediblesetThresh> <minPPA> [-chr <str>] [-max_num_signif <int>] [-num_signif <int> ] [-clean]
#
#   description:
#       Takes a CSV file output by pygwas convert and outputs a BED file for
#       SNPs with p-values smaller than alpha and MACs greater than macsCutoff.
#       If bonferroni is specified then the bonferroni adjusted p-value is
#       calculated based on number of snps in whole file and an alpha of alpha,
#       likewise, bh calculates FDR, and if raw is specified the value of alpha
#       is used directly. <distance> is the maximum distance from the lead snp
#       to search for other snps. <bfprocessors> is the number of parallel jobs
#       for running bayes factor/ppa calculations. snpVCFbgz must be the relative
#       path to the actual file, not a symlink. <featfile> needs to be a bed
#       e.g. containing TFBS with feature name in column 4. <crediblesetThresh>
#       should be a percent, e.g. 98.5. <minPPA> is the minimum PPA for a
#       credible set as a percent. Use -chr to run on only 1 chromosome. Use
#       -num_signif to modulate significance threshold to only get <int>
#       significant SNPs with a ceiling of the specified FDR bonferroni,
#       or p-value threshold. If -clean is specified then the working dir will be
#       removed.

import sys
import os
import io
import pandas as pd
import math
import sumstats
from statsmodels.stats.multitest import fdrcorrection
from subprocess import run
from multiprocessing import Pool, Process
from shutil import copyfile

def fine_map(csvDf, lead_snp_row):

    chrname = str(lead_snp_row["chromosomes"].astype(int))

    # Return if not chr selected by CL flag -chr
    if getchr and chrname != chrselect:
        return

    pos = str(lead_snp_row["positions"].astype(int))

    snpname = f"{chrname}_{pos}"

    # Get snps within <distance> of each significant snp
    print(f"Subsetting snps within {distance}bp of {snpname}")

    signifsnpname = "_".join([chrname, pos])
    closestSnpsDf = csvDf[(csvDf["chromosomes"] == lead_snp_row["chromosomes"]) & (distance >= abs(csvDf["positions"] - lead_snp_row["positions"]))].copy()

    # Add name col
    closestSnpsDf["name"] = closestSnpsDf[["chromosomes", "positions"]].astype(str).agg("_".join, axis = 1)

    # Add bonferroni col
    closestSnpsDf["bonferroni"] = closestSnpsDf["pvals"] * num_snps

    # Make df for bed file
    bedDf = pd.DataFrame()
    bedDf["chrom"] = closestSnpsDf["chromosomes"]
    bedDf["start"] = closestSnpsDf["positions"] - 1
    bedDf["end"] = closestSnpsDf["positions"]
    bedDf["name"] = closestSnpsDf["name"]

    # Sort bed dataframe
    bedoutput = f"{signifsnpname}.closest_{distance}bp.bed"
    bedDf.sort_values(["chrom", "start"], axis = 0, inplace = True)
    bedDf.to_csv(f"{bedoutput}", sep = "\t", header = False, index = False)

    # Extract region from SNP VCF
    print(f"Subsetting {snpVCFbgz} for snps")
    VCFoutput = f"{bedoutput.rstrip('.bed')}.vcf"
    with open(VCFoutput, "w") as outf:
        run(["tabix", "-h", f"../../{snpVCFbgz}", "-R", bedoutput], stdout = outf)

    # Annotate SNP with genomic context (e.g. intron, exon) and effect (e.g. nonsynonomous mutation)
    VCFannot = f"{VCFoutput.rstrip('.vcf')}.snpeff_annot.vcf"
    snpeffcall = ["snpEff", "Arabidopsis_thaliana", VCFoutput]
    print(f"Running snpEff on {VCFoutput}")
    run(snpeffcall, stdout = open(VCFannot, "w"))

    # Extract annotations with snpSift
    #1   29776102    downstream_gene_variant,downstream_gene_variant,intron_variant  -1,-1,6
    #SnpSift extractFields -s "," -e "." snpEff.annotated.vcf CHROM POS ANN[*].EFFECT ANN[*].RANK
    SnpSiftFl = f"{VCFannot.rstrip('.vcf')}.snpsift_annot.tsv"
    snpsiftcall = ["SnpSift", "extractFields", "-s", ",", "-e", ".", VCFannot, "CHROM", "POS", "ANN[*].EFFECT", "ANN[*].RANK", "ANN[*].HGVS_P", "ANN[*].GENEID"]
    print(f"Running snpSift on {VCFannot}")
    with open(SnpSiftFl, "w") as outf:
        run(snpsiftcall, stdout = outf)

    # Parse SnpSift output (variant type and rank (e.g. 4th exon)
    variantTypeDict = dict()
    with open(SnpSiftFl) as inf:
        for line in inf:
            if line.startswith("CHROM"):
                continue
            chrom, snpPos, vtypes, ranks, effs, genes = line.strip().split("\t")
            # Adjust snp position to match pygwas file
            #pos = int(pos) + 1
            snpname = f"{chrom}_{snpPos}"
            ranksList = ranks.split(",")
            vtypesList = vtypes.split(",")
            effsList = effs.split(",")
            genesList = genes.split(",")
            # Keep only in-gene variant. Note otherwise as intergenic
            vtList = [vt for vt in vtypesList if vt not in ("downstream_gene_variant", "upstream_gene_variant", "intergenic_region")]
            if len(vtList) == 0:
                vtypeStrList = ["intergenic"]
                rankStrList = ["NA"]
                effStrList = ["NA"]
                geneStrList = ["NA"]
                #vtype = "intergenic"
                #rank = "NA"
                #eff = "NA"
                #gene = "NA"
            else:
                vtypeStrList = list()
                rankStrList = list()
                effStrList = list()
                geneStrList = list()
                for vtype in vtList:
                    vtypeIndx = vtypesList.index(vtype)
                    vtypeStrList.append(vtypesList[vtypeIndx])
                    rankStrList.append(ranksList[vtypeIndx])
                    effStrList.append(effsList[vtypeIndx])
                    geneStrList.append(genesList[vtypeIndx])
            vtypeStr = "|".join(vtypeStrList)
            rankStr = "|".join(rankStrList)
            effStr = "|".join(effStrList)
            geneStr = "|".join(geneStrList)
            variantTypeDict[snpname] = {"variant":vtypeStr, "rank":rankStr, "effect":effStr, "gene":geneStr}

    print("Writing csv with analysis info")
    tmpDat = list()
    for i, row in closestSnpsDf.iterrows():
        snpname = row["name"]
        #inblock = True if str(row["positions"]) in snpsPosInBlocks else False
        leadsnp = True if str(row["positions"]) == pos else False
        if snpname in variantTypeDict:
            variant = variantTypeDict[snpname]["variant"]
            rank = variantTypeDict[snpname]["rank"]
            eff = variantTypeDict[snpname]["effect"]
            gene = variantTypeDict[snpname]["gene"]
        else:
            variant = "NA"
            rank = "NA"
            eff = "NA"
            gene = "NA"
        #extRow = list(row) + [inblock, leadsnp, variant, rank, eff, gene]
        extRow = list(row) + [leadsnp, variant, rank, eff, gene]
        tmpDat.append(",".join([str(item) for item in extRow]))
    with open(f"{signifsnpname}.closest.tmp.csv", "w") as outf:
        # Write header
        #outf.write(",".join(closestSnpsDf.columns) + ",inblock,leadsnp,variant,rank,effect,gene\n")
        outf.write(",".join(closestSnpsDf.columns) + ",leadsnp,variant,rank,effect,gene\n")
        outf.write("\n".join(tmpDat))

def run_approx_lnbf(data):
    """"
    Function for running approx_lnbf on a single set of pval, maf, N. data should be
    a tuple or list containing pval, maf, and N in that order.
    """
    pval, maf, N = data
    bf = sumstats.approx_lnbf(pval = pval, freq = maf, sample_size = N) 
    return(bf)

def calc_ppa(data):
    """
    Function to calculate posterior probability of association from a list of
    bayes factors. data should be a tuple or list containing the bayes factors
    and normalizing coefficients, in that order.
    """
    lnbf = data[0]
    normalizing_coefficient = data[1]
    ppa = math.exp(lnbf - normalizing_coefficient)
    return(ppa)

def addFeats(bedfile, featfile, scriptdir, infilePrefix):
    basename = os.path.basename(bedfile).rstrip(".bed")
    outfname = f"annot_beds_with_tfbs/{basename}_with_other_feats.bed"
    with open(outfname, "w") as outf:
        run([f"{scriptdir}/add_intersect_feat_to_bed.py", "-bed", bedfile, "-feat", featfile], stdout = outf)

def getCredibleSet(bedfile, csThresh, minPPA):
    basename = os.path.basename(bedfile).rstrip(".bed")
    outfname = f"credible_set_beds/{basename}_minPPA_{minPPA}_{csThresh}_credible_set.bed"
    with open(outfname, "w") as outf:
        run([f"{scriptdir}/bed_ppa_to_credible_set.py", "-bed", bedfile, "-csThresh", csThresh, "-minPPA", minPPA], stdout = outf)
    

# Get params
args = sys.argv

filename = args[1]
method = args[2]
alpha = float(args[3])
macsthresh = int(args[4])
distance = int(args[5])
snpVCFbgz = args[6]
processes = int(args[7])
bfprocs = int(args[8])
featfile = os.path.abspath(args[9])
csThresh = args[10]
minPPA = args[11]

if "-chr" in args:
    getchr = True
    chrselect = args[args.index("-chr") + 1]
else:
    getchr = False

if "-num_signif" in args:
    set_signif = True
    num_signif = int(args[args.index("-num_signif") + 1])
else:
    set_signif = False

if "-max_num_signif" in args:
    set_max_signif = True
    max_num_signif = int(args[args.index("-max_num_signif") + 1])
else:
    max_num_signif = False

infiledir = os.path.dirname(os.path.abspath(filename))
scriptdir = os.path.dirname(os.path.realpath(__file__))

# Read csv
print("Reading pygwas csv file")
csvDf = pd.read_csv(filename, header = 0)

# Get prefix (gene name)
infilePrefix = os.path.basename(filename.rstrip(".csv"))

# Get #snps for calculating bonferroni adjusted p-values
num_snps = csvDf.shape[0]

# Add fdr and bonferroni-adjusted p-values to table
print("Adding FDR column")
csvDf["fdr"] = fdrcorrection(csvDf["pvals"], alpha =  alpha)[1]

# Subset input df for those passing macs threshold
print("Applying MACs threshold")
csvDf = csvDf[csvDf["macs"] > macsthresh]

# Subset for significant snps
print("Subsetting pygwas csv for significant snps")

## Select SNPs passing threshold specified
#if method == "bh":
#    signifDf = csvDf[(csvDf["fdr"] < alpha) & (csvDf["macs"] > macsthresh)]
#elif method == "bonferroni":
#    signifDf = csvDf[((csvDf["pvals"] * num_snps) < alpha) & (csvDf["macs"] > macsthresh)]
#else:
#    signifDf = csvDf[(csvDf["pvals"] < alpha) & (csvDf["macs"] > macsthresh)]

# Select SNPs passing threshold specified
if method == "bh":
    signifDf = csvDf[csvDf["fdr"] < alpha]
elif method == "bonferroni":
    signifDf = csvDf[(csvDf["pvals"] * num_snps) < alpha]
else:
    signifDf = csvDf[csvDf["pvals"] < alpha]

# Select num_signif snps with lowest pvals
if set_signif:
    # If fewer than -num_signif snps in data frame, subset full dataframe for
    # -num_signif with lowest p-val
    print(f"Selecting top {num_signif} SNPs")
    if signifDf.shape[0] < num_signif:
        signifDf = csvDf.sort_values(by = "pvals", axis = 0, ascending = True).loc[range(num_signif)]
    else:
        # Sort df by pval
        signifDf = signifDf.sort_values(by = "pvals", axis = 0, ascending = True)
        signifDf.reset_index(inplace = True, drop = True)
        signifDf = signifDf.loc[range(num_signif)]

# Select at most max_num_signif SNPs
elif set_max_signif:
    if signifDf.shape[0] == 0:
        print("No significant SNPs")
        pass
    elif signifDf.shape[0] < max_num_signif:
        print(f"{signifDf.shape[0]} significant SNPs")
        pass
    else:
        print(f"{signifDf.shape[0]} significant SNPs. Selecting top {max_num_signif}")
        # Sort df by pval
        signifDf = signifDf.sort_values(by = "pvals", axis = 0, ascending = True)
        signifDf.reset_index(inplace = True, drop = True)
        signifDf = signifDf.loc[range(max_num_signif)]

# Make workingdir if it doesn't exist
workingdir = f"{infiledir}/workingdir/"
if not os.path.exists(workingdir):
    os.makedirs(workingdir)

# Move to workingdir
os.chdir(workingdir)

# Make VCF index if not done already
if not os.path.exists(f"../{snpVCFbgz}.tbi"):
    print(f"Indexing VCF: {snpVCFbgz}")
    run(["tabix", "-fp", f"../{snpVCFbgz}"])

# For each significant snp, make bed file containing only snps within <distance>bp
# on either side, use it to subset the VCF for the snps

# Make gene workingdir if it doesn't exist
if not os.path.exists(infilePrefix):
    os.makedirs(infilePrefix)

# Move to gene workingdir
os.chdir(infilePrefix)

# Run process
rows = [row for i, row in signifDf.iterrows()]
numrows = len(list(rows))

with Pool(processes) as pool:
    pool.starmap(fine_map, zip([csvDf] * numrows, rows))

os.chdir(f"{workingdir}/{infilePrefix}")

if not os.path.exists("annot_beds"):
    os.makedirs("annot_beds")

for tmpfile in [f for f in os.listdir() if f.endswith(".closest.tmp.csv")]:
    # Calculate bayes factors and posterior probability of association
    # Arrange pval, mafs, N for run_approx_lnbf multiprocessing

    basename = tmpfile.rstrip(".closest.tmp.csv")
    closestSnpsDf = pd.read_csv(tmpfile, header = 0)
    p_values = closestSnpsDf["pvals"]
    Nlist = [num_snps] * num_snps
    mafs = closestSnpsDf["mafs"]
    data = list(zip(p_values, mafs, Nlist))

    # Calculate log bayes factors
    print("Calculating bayes factors")
    with Pool(bfprocs) as p:
        lnbfs = p.map(run_approx_lnbf, data)
    closestSnpsDf["lnbf"] = lnbfs

    # Prepare list of lists to run calc_ppa multiprocessing
    normalizing_coefficient = sumstats.log_sum(lnbfs)
    ncList = [normalizing_coefficient] * num_snps
    data = list(zip(lnbfs, ncList))

    # Calculate ppa
    print("Calculating posterior probability of association")
    with Pool(bfprocs) as p:
        ppas = p.map(calc_ppa, data)
    closestSnpsDf["ppa"] = ppas

    # Making BED file
    print("Writing bed file")
    bedDatList = list()
    for i, row in closestSnpsDf.iterrows():
        attr = f"ID={infilePrefix}_{row['name']};Name={row['name']};pval={row['pvals']};bonferroni_pval={row['bonferroni']};fdr={row['fdr']};mafs={row['mafs']};macs={row['macs']};lnbf={row['lnbf']};ppa={row['ppa']};leadSNP={row['leadsnp']};variant={row['variant']};rank={row['rank']};effect={row['effect']};gene={row['gene']}"
        bedLine = f"{row['chromosomes']}\t{row['positions'] - 1}\t{row['positions']}\t{row['name']}\t{row['ppa']}\t.\tpygwas\tsnp\t{attr}"
        bedDatList.append(bedLine)

    bedDatStr = "\n".join(bedDatList)
    bedDatDf = pd.read_csv(io.StringIO(bedDatStr), header = None, sep = "\t")
    bedDatDf.sort_values([4], axis = 0, inplace = True, ascending = False)
    outfname = f"annot_beds/{infilePrefix}_{basename}_closest_{distance}_signifSNP_annot.bed"
    bedDatDf.to_csv(outfname, sep = "\t", header = False, index = False)


if not os.path.exists("annot_beds_with_tfbs"):
    os.makedirs("annot_beds_with_tfbs")

# Add TFBS to bed files
print("Annotating TFBS")
annotbedfiles = [f"annot_beds/{f}" for f in os.listdir("annot_beds")]
numfiles = len(annotbedfiles)
with Pool(processes) as pool:
    pool.starmap(addFeats, zip(annotbedfiles, [featfile] * numfiles, [scriptdir] * numfiles, [infilePrefix] * numfiles))

if not os.path.exists("credible_set_beds"):
    os.makedirs("credible_set_beds")

# Get credible sets of files
print(f"Getting {csThresh}% credible sets with min PPA of {minPPA}")
annotbedfiles = [f"annot_beds_with_tfbs/{f}" for f in os.listdir("annot_beds_with_tfbs")]
numfiles = len(annotbedfiles)
with Pool(processes) as pool:
    pool.starmap(getCredibleSet, zip(annotbedfiles, [csThresh] * numfiles, [minPPA] * numfiles))

csdir = f"{infiledir}/credible_sets"
if not os.path.exists(csdir):
    os.makedirs(csdir)

csbedfiles = [f"credible_set_beds/{f}" for f in os.listdir("credible_set_beds")]
for f in csbedfiles:
    copyfile(f, f"{csdir}/{os.path.basename(f)}")

# Remove temp dir
if "-clean" in args:
    os.rmdir("workingdir")
# VCF file example first four columns
#CHROM	POS	ID	REF
#1	6140322	.	A
#1	6140341	.	T
#1	6140342	.	G
#1	6140421	.	A
#1	6140422	.	A
#1	6140429	.	A
#1	6140430	.	T
#1	6140444	.	G
#1	6140445	.	T

# .CHECK file example
##	Name	Position	ObsHET	PredHET	HWpval	%Geno	FamTrio	MendErr	MAF	Alleles	Rating	
#1	.	25970729	0.0	0.004	1.2397E-6	97.3	0	0	0.002	C:A	
#2	..1	25970756	0.0	0.002	9.0E-4	98.9	0	0	0.001	A:G	BAD
#3	..2	25970759	0.0	0.002	9.0E-4	98.7	0	0	0.001	C:A	BAD
#4	..3	25970760	0.0	0.002	9.0E-4	99.1	0	0	0.001	A:G	BAD
#5	..4	25970763	0.0	0.023	4.5541E-31	99.3	0	0	0.012	C:T	
#6	..5	25970800	0.0	0.002	9.0E-4	98.0	0	0	0.001	C:T	BAD
#7	..6	25970838	0.0	0.167	3.5496E-148	98.0	0	0	0.092	G:C	
#
# .blocks file example:
#BLOCK 1.  MARKERS: 7 19 46 49 61 79
#313241 (0.522)	|0.296	0.188	0.025	0.006|
#313141 (0.184)	|0.073	0.000	0.111	0.000|
#313121 (0.076)	|0.018	0.000	0.000	0.060|
#323241 (0.066)	|0.000	0.068	0.000	0.000|
#311241 (0.059)	|0.059	0.000	0.000	0.000|
#213144 (0.051)	|0.000	0.000	0.000	0.052|
#213141 (0.042)	|0.043	0.000	0.000	0.000|
#Multiallelic Dprime: 0.592
#BLOCK 2.  MARKERS: 94 109 116



    # Select SNPs that intersect a gene or TFBS+DAP-seq peak (other script)

    # Calculate bayes factor and posterior probability of association (PPA) (sumstats)

    # Find credible sets based on PPA

## example input csv
#chromosomes,positions,pvals,mafs,macs,genotype_var_perc
#1,10000041,0.651017342196,0.0015243902439,1,0.00031304359436
#1,10000121,0.203100521149,0.0457317073171,30,0.00247579813004
#1,10000135,0.836016636409,0.00609756097561,4,6.55651092529e-05
#1,10000153,0.663064175281,0.00609756097561,4,0.00029045343399
#1,10000173,0.878296598174,0.147865853659,97,3.58819961548e-05
#1,10000185,0.423354160554,0.0015243902439,1,0.000980377197266
#1,10000189,0.158263097194,0.0015243902439,1,0.00304162502289
#1,10000226,0.4334977027,0.00457317073171,3,0.000938236713409
#1,10000303,0.771673786838,0.00914634146341,6,0.000128865242004
